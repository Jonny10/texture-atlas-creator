﻿using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class TextureExample : MonoBehaviour
{
    [Header("Input")]
    public Texture2D[] textures;
    [Range(0f,1f)]
    public float[] scales;

    [Header("Output")]
    public Texture2D atlas;
    public Material mat;

    [ContextMenu("PACK IT UP")]
    public void Pack()
    {
        if(textures.Length == 0)
        {
            Debug.LogError("No textures to pack");
            return;
        }

        if(scales.Length != textures.Length)
        {
            Debug.LogError("Scales array does not match texture array in length");
            return;
        }

        //Compose a new array of AtlasItems
        TextureAtlas.AtlasItem[] items = new TextureAtlas.AtlasItem[textures.Length];

        for (int i = 0; i < items.Length; i++)
        {
            items[i] = new TextureAtlas.AtlasItem
            {
                tex = textures[i],
                resolution = TextureAtlas.ScaleToResolution(scales[i])
            };
        }

        atlas = TextureAtlas.Create(items);

        if (mat) mat.mainTexture = atlas;
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(TextureExample))]
public class TextureExampleEditor : Editor
{
    private TextureExample script;
    private bool autoRegenerate;

    void OnEnable()
    {
        script = (TextureExample)target;
    }

    override public void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck();

        DrawDefaultInspector();

       
        EditorGUILayout.Space();

        if (EditorGUI.EndChangeCheck())
        {
            if(autoRegenerate) script.Pack();
        }

        using (new EditorGUILayout.HorizontalScope())
        {
            if (GUILayout.Button("Create atlas"))
            {
                script.Pack();
            }
            autoRegenerate = EditorGUILayout.ToggleLeft("Auto", autoRegenerate);
            GUILayout.FlexibleSpace();

        }

        DrawTexturePreviews();
    }

    private void DrawTexturePreviews()
    {
        EditorGUILayout.Space();
        string res = ((script.atlas) ? script.atlas.height.ToString() +"x"+ script.atlas.width.ToString() : " ");
        EditorGUILayout.LabelField("Output " + res + "px", EditorStyles.boldLabel);

        Rect cRect = EditorGUILayout.GetControlRect();
        using (new EditorGUILayout.HorizontalScope())
        {
            if (script.atlas) EditorGUI.DrawPreviewTexture(new Rect(cRect.x, cRect.y, 150f, 150f), script.atlas, null, ScaleMode.ScaleToFit);

        }
        GUILayout.Space(140f);
    }
}
#endif