﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureAtlas
{
    //Conditional defines for platform here
#if UNITY_IOS || UNITY_ANDROID
    private const int MAX_ATLAS_SIZE = 1024;
#else
    private const int MAX_ATLAS_SIZE = 4096;
#endif

    //Min/max resolution for atlas items
    private const int MIN_RES = 128;
    private const int MAX_RES = 1024;

    public static Texture2D atlas;

    //Class to act as packing instructions
    public class AtlasItem
    {
        public Texture2D tex;
        public int resolution = 256;
    }

    private static Texture2D[] textures;
    private static Rect[] coords;

    public static Texture2D Create(AtlasItem[] items)
    {
        CreateTextureArray(items);

        PackTextures();

        return atlas;
    }

    private static void CreateTextureArray(AtlasItem[] items)
    {
        textures = new Texture2D[items.Length];

        for (int i = 0; i < items.Length; i++)
        {
            textures[i] = items[i].tex;

            //Downscale texture
            if (textures[i].width > items[i].resolution)
            {
                //Passing a texture through the GPU also means the source file doesn't need to have the "Read/Write" flag enabled
                //Creates unique texture, duplicates of the same size won't be removed because of this, which isn't ideal
                textures[i] = ResizeTexture(textures[i], items[i].resolution);
            }
        }
    }

    private static float Remap(float val, float minIn, float maxIn, float minOut, float maxOut)
    {
        return minOut + (val - minIn) * (maxOut - minOut) / (maxIn - minIn);
    }

    //Convert a 0-1 value to a texture resolution
    public static int ScaleToResolution(float scale)
    {
        scale = Mathf.Clamp01(scale);

        int resolution = (int)Remap(scale, 0, 1f, MIN_RES, MAX_RES);

        resolution = Mathf.ClosestPowerOfTwo(resolution);

        //Debug.Log(scale + " > " + resolution);

        return resolution;

    }

    public static Texture2D ResizeTexture(Texture2D src, int resolution)
    {
        Rect texRect = new Rect(0, 0, resolution, resolution);

        RenderTexture rt = new RenderTexture(resolution, resolution, 32, RenderTextureFormat.ARGB32);
        rt.useMipMap = false;

        Graphics.SetRenderTarget(rt);

        GL.LoadPixelMatrix(0, 1, 1, 0);

        GL.Clear(true, true, Color.clear);
        Graphics.DrawTexture(new Rect(0, 0, 1, 1), src);

        Texture2D result = new Texture2D(resolution, resolution, TextureFormat.RGB24, false, true);

        result.ReadPixels(texRect, 0, 0, false);

        result.Apply();

        return result;
    }

    private static void PackTextures()
    {
        atlas = new Texture2D(MIN_RES, MIN_RES, TextureFormat.RGB24, false);
        atlas.name = "TextureAtlas";

        coords = new Rect[textures.Length];
        coords = atlas.PackTextures(textures, 0, MAX_ATLAS_SIZE);
    }

}



